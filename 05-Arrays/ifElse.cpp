/**********************************************
* File: ifElse.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains an example of an if/else statement 
* using floats and doubles, to show the student the 
* importance of understanding how the data in floats 
* and doubles are structured.
**********************************************/
#include <iostream>
#include <iomanip>

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	/* Variable Declarations */
	float x = (float)1.1, y = (float)1.2;
	double x2 = 1.3, y2 = 1.4;
	
	/* Evaluae if statements */
	if( x == y ){
		std::cout << "The statement x==y is true\n" << char(10);
	}
	else{
		std::cout << "The statement x==y is false\n" << std::endl;
	}
	
	/* This if/else will select the final else statement */
	if( x2 == y2 ){
		std::cout << "The statement x2==y2 is true" << (char)10;
	}
	else if( (y2 - x2) == 0.1 ){
		std::cout << "The statement (y2 - x2) == 0.1 is true\n";
	}
	else{
		std::cout << "Both are false!\n"; 
		
		std::cout << "If you print double with a precision of 6, it will look right, and you'll see "
			<< std::setprecision(6) << y2-x2 << std::endl;
		std::cout << "But with a precision of 20, you'll see the true value of "
			<< std::setprecision(20) << y2-x2 << std::endl;
	}
	
	return 0;
}

