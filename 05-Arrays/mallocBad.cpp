/**********************************************
* File: mallocBad.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains an example of improper memory allocation
* Here, we allocate the memory for a character array
* However, we define the char* like a string
* This reallocates the memory, and frees the pointer
* The later free will cause a memory trace because the pointer 
* is no longer pointing to a memory location 
**********************************************/

#include <iostream>
#include <cstdlib>

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){

	/* Properly allocate memory */
	long unsigned int mallocSize = 5;
	char *First = (char *)malloc(mallocSize * sizeof(char));
	
	std::cout << "Location of First is " << (void *)First << std::endl;  
	
	/* Incorrect insertion of elements */
	First = "Notre";
	
	std::cout << "The word is " << First << std::endl;

	/* Free Memory */
	free(First);


}
