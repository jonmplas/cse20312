#include "../classes/LinearProbe.h"
#include "../classes/QuadProbe.h"

template<class Key, class Value>
void printStatus(const HashTable<Key, Value>&  linearHash,
				 const QuadProbe<Key, Value>& quadHash ){
	
	std::cout << "Linear Hash Results: " << std::endl;
	std::cout << linearHash << std::endl << std::endl;
	
	std::cout << "Quadratic Hash Results: " << std::endl;
	std::cout << quadHash << std::endl << std::endl;
	
}

int main(){
	
	HashTable<int, int> linearHash(7);
	QuadProbe<int, int> quadHash(7);
	
	linearHash.insert(76);	quadHash.insert(76);
	linearHash.insert(40);	quadHash.insert(40);
	linearHash.insert(48);	quadHash.insert(48);
	linearHash.insert(5);	quadHash.insert(5);
	linearHash.insert(12);	quadHash.insert(12);
	
	printStatus(linearHash, quadHash);
	
	return 0;
}

