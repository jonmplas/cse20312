#include <iostream>

float halfFloat(float tempFloat){
    tempFloat /= 2;

    return tempFloat;
}

float halfFloat(float& tempFloat){
    tempFloat /= 2;

    return tempFloat;
}

float halfFloat(const float tempFloat){
    tempFloat /= 2;

    return tempFloat;
}

int main(){

    float theFloat =  (float)2.7;
    std::cout << halfFloat( theFloat ) << std::endl;    

    return 0;
}