/**********************************************
* File: Money.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Class Declaration for Money 
**********************************************/

#ifndef MONEY_H
#define MONEY_H

#include <iostream>

class Money{

	private:
		long int dollars;
		int cents;
		
	public:
	
		Money(long int dollarsIn, int centsIn);
		
		Money operator+(const Money& rhs);

		Money operator-(const Money& rhs);
		
		friend std::ostream& operator<<(std::ostream& out, const Money& print);

};

#endif
