/**********************************************
* File: AnagramComp.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Takes in two strings from the command line and 
* determines if the strings are anagrams 
**********************************************/

#include "../classes/DoubleHash.h"

const unsigned int numASCII = 128;

void initializeHash(DoubleHash<char, int>& theHash){
	
	for(unsigned int iter = 0; iter < numASCII; iter++){
		// Initialize each ASCII in the Hash to 0
		theHash.insert( { (char)iter, 0 } );
	}
}

void hashWord(DoubleHash<char, int>& theHash, char* theWord){
	
	for(unsigned int iter = 0; *(theWord + iter) != (char)0; iter++){
		theHash[ theWord[iter] ]++;
	}
}


bool compareHash(DoubleHash<char, int>& wordHash1, DoubleHash<char, int>& wordHash2){
	
	for(unsigned int iter = 0; iter < numASCII; iter++){

		if( wordHash1[ (char)iter ] != wordHash2[ (char)iter ] ){
			return false;
		}
	}

	return true;
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(int argc, char** argv){

	if(argc != 3){
		std::cerr << "Inputs should be \"./RemoveDups [String]\"\n"; 
		exit(-1);
	}
	
	std::cout << "Comparing the following for anagrams: \"" << argv[1];
	std::cout << "\" and \"" << argv[2] << "\"" << std::endl;
	
	DoubleHash<char, int> wordHash1(numASCII);
	DoubleHash<char, int> wordHash2(numASCII);
	
	initializeHash(wordHash1);
	initializeHash(wordHash2);
	
	hashWord(wordHash1, argv[1]);
	hashWord(wordHash2, argv[2]);
	
	if( compareHash(wordHash1, wordHash2) ){
		std::cout << "The words ARE anagrams!" << std::endl;
	}
	else{
		std::cout << "The words are not anagrams!" << std::endl;
	}
	
	return 0;
}
